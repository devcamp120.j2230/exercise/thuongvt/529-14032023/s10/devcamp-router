
import { Route, Routes } from 'react-router-dom';
import './App.css';
import FirstPage from './pages/FirstPage';
import HomePage from './pages/HomePage';
import SecondPage from './pages/SecondPage';
import ThridPage from './pages/ThridPage';

function App() {
  return (
    <div>
  <Routes>
    <Route path='/' element={<HomePage/>}></Route>
    <Route path='/fristPage' element={<FirstPage/>}></Route>
    <Route path='/secondPage' element={<SecondPage/>}></Route>
    <Route path='/thirdPage' element={<ThridPage/>}></Route>
    <Route path='*' element={<HomePage/>}></Route>
  </Routes>
    </div>
  );
}

export default App;
