import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";

function SecondPageContent (){
    return (
      <Container>
        <Grid container>
            <Grid item>
                <Typography >
                    <h4>
                        Second Page
                    </h4>
                </Typography>
            </Grid>
        </Grid>
      </Container>  
    )
}

export default SecondPageContent