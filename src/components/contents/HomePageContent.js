import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";

function HomePageContent (){
    return (
      <Container>
        <Grid container>
            <Grid item>
                <Typography >
                    <h4>
                        Home Page
                    </h4>
                </Typography>
            </Grid>
        </Grid>
      </Container>  
    )
}

export default HomePageContent