import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";

function FirstPageContent (){
    return (
      <Container>
        <Grid container>
            <Grid item>
                <Typography >
                    <h4>
                        First Page
                    </h4>
                </Typography>
            </Grid>
        </Grid>
      </Container>  
    )
}

export default FirstPageContent