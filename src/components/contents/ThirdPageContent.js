import { Grid, Typography } from "@mui/material";
import { Container } from "@mui/system";

function ThirdPageContent (){
    return (
      <Container>
        <Grid container>
            <Grid item>
                <Typography >
                    <h4>
                        Third Page
                    </h4>
                </Typography>
            </Grid>
        </Grid>
      </Container>  
    )
}

export default ThirdPageContent