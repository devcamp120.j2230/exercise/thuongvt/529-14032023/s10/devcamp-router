import ThirdPageContent from "../components/contents/ThirdPageContent"
import Header from "../components/Header"

function ThridPage (){
    return (
        <>
        <Header></Header>
        <ThirdPageContent></ThirdPageContent>
        </>
    )
}
export default ThridPage