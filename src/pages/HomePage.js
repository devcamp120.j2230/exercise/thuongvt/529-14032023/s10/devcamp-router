import HomePageContent from "../components/contents/HomePageContent"
import Header from "../components/Header"

function HomePage  (){
    return (
        <>
        <Header></Header>
        <HomePageContent></HomePageContent>
        </>
    )
}
export default HomePage