import FirstPageContent from "../components/contents/FirstPageContent"
import Header from "../components/Header"

function FirstPage  (){
    return (
        <>
        <Header></Header>
        <FirstPageContent></FirstPageContent>
        </>
    )
}
export default FirstPage