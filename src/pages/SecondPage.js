import SecondPageContent from "../components/contents/SecondPageContent"
import Header from "../components/Header"

function SecondPage  (){
    return (
        <>
        <Header></Header>
        <SecondPageContent></SecondPageContent>
        </>
    )
}
export default SecondPage